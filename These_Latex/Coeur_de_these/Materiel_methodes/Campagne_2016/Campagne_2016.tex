\section{Campagne UECOCOT 2016}

\subsection{General setting of the campaign}

The UECOCOT ( \emph{Usine, Environnement et COntamination en zone COTière}) campaign was set up to study the swell transformation across the reef in the Ouano lagoon, and its effect on the general circulation of the lagoon. To do so, several arrays of sensors were deployed. Pressure sensors were used for the swell, the infragravity waves, the very low frequency waves and the mean level variations, and ADCP sensors were used for both temperature and velocity on the water column.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Position_Ouano.png} 
\includegraphics[width=0.3\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Ellipse.png} 
\caption{Position of the Ouano lagoon in New Caledonia and position of the ADCP sensor in the lagoon. }
\label{PositionOuano}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Pressure_sensors_Sous_2020.png} 
\caption{Position of the different pressure sensors. Taken from Sous et al. ( in review)}
\label{Positionpressuresensor}
\end{figure}

Other data could be taken from the existing sensors, such as the temperature or the turbidity. Those could be used to estimate the origin of water entering the lagoon, since water is expected to have different and turbidity patterns depending on its origin. We expect oceanic waters to have lower temperature, because water column is deeper in the ocean and therefore the sun energy is diluted in a greater volume. However, the same argument holds true for the wind cooling, and therefore the pattern could be more complex. Even more, the presence of underwater sources in lagoon has already been reported and could be the cause for variations of temperature as well. One final reason could be the impact of precipations.

Even more, all this is likely to depend from the temperature of oceanic water. And those oceanic waters could be influenced by wind-induced upwellings. This all leads to the necessity to consider at least some larger scales factors, such as the wind intensity, direction and the air temperature. Those data are not directly available on site but were gathered from the closest airport, la Tontouta. 

Finally, an important point remain. The wave climate is highly important in the circulation because the breaking waves on the barreer reefs transfer momentum from the oceanic waves to the general circulation. This transfer of momentum contributes to stronger fluxes of water from the ocean to the lagoon, depending on the intensity of those waves. But also depending, as we will show, on other factors such as the water level modulation by the tidal cycle.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Difference_deferlement.png} 
\caption{Effect on the wave-breaking on the barreer reef. Also, we see from the composite image the difference in breaking at different times.}
\label{Breaking_barreer_difference}

\end{figure}

But all water does not come through the barreer. Some also comes from the passes. Since those passes are deep, we could expect a three dimensionnal circulation in those. This would make the whole issue more complex. One additional layer of complexity would come from the impact of the waves shoaling in those passes, since they do not always break there. \textcolor{blue}{Therefore, the shear stress of the current should be evaluated in those passes.}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Diffraction_passes_Ouarai_Isie.png} 
\caption{Diffracting waves in some passes of the lagoon}
\label{Diffraction_passes}
\end{figure}

\subsection{Temporal evolution of the different quantities}

Here we present the temporal variations for the different quantities during the 2016 campaign.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Air_temperature.png} 
\caption{Evolution of the air temperature in degrees during the campaign}
\label{airtempevol}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Evolution_elevation_gaussian_filter_20_sigma.png} 
\caption{Evolution of the mean water level during the campaign}
\label{waterlevelevol}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Evolution_waves.png} 
\caption{Evolution of the significant wave height during the campaing}
\label{waveevol}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Infragravity_wave_evolution_gaussian_10_sigma.png} 
\caption{Evolution of the significant wave height for infragravity waves during the campaign}
\label{infragravityevol}
\end{figure}

In the figure \ref{infragravityevol} we see an interesting pattern that we see for all components of waves. We see that the signal are correlate between all sensors, but that the significant wave height decreases while the waves progress in the lagoon.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Swell_evolution_gaussian_filtering_10_sigma.png} 
\caption{Evolution of the significant wave height for the swell during the campaign}
\label{swellevol}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Variation_atmospheric_pressure.png} 
\caption{Evolution of the atmospheric pressure during the campaign}
\label{atmpressevol}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/Wind_direction_time.png} 
\caption{Evolution of the wind direction during the campaign}
\label{evolwinddir}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Time_evolution/wind_velocity.png} 
\caption{Evolution of the wind velocity during the campaign}
\label{evolwindintensity}
\end{figure}

\subsection{Spectral variations of the quantities}

\textcolor{blue}{\large Mean water level}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Overelevation_spectrum_blackman_gaussian_3_sigma.png} 
\caption{ Spectrum of the water level variations during the campaing, with a blackman filter and smoothed with a gaussian filter with 3 sigma width}
\label{spectrummeanwaterlevel}
\end{figure}

In this spectrum we see that the constant term is the dominant one. But we also see diurnal and semi-diurnal variations, and other compound tides. We see that the semi diurnal variations are dominant. 

If we remove the blackman filter, we can distinguish the O1 and K1 contributions and the M2 and S2 contributions. 

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Spectrum_M2_S2_336_padding_without_blackman.png} 
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Spectrum_O1_K1_336_padding_without_blackman.png}
\caption{Zoom on the 1) Semi-diurnal peak and 2) diurnal peak, where we can clearly distinguish the M2,S2 and O1, K1 contributions}
\label{Distinguishingtidaldiurnal} 
\end{figure}


\subsubsection{Waves}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Spectrum_wave_offshore_blackman_pad_56.png} 
\caption{Offshore waves spectrum from the WW3 model and the S4 sensor.}
\label{offshorewavespectrum}
\end{figure}

In the figure \ref{offshorewavespectrum} we see the wave significant height spectrum from an oceanic spectrum Wavewatch3 and on the reef with the S4 sensor. The striking feature is the appearance of a semi-diurnal peak in the S4 signal, which was not present before. Therefore, we can expect the mean level or the tidal currents to influence the wave propagation in the lagoon. Interestingly, this component is mainly semi-diurnal and not extremely strong. We also see that the decrease of the power spectrum in high frequencies is different for the oceanic Wavewatch 3 model and the S4 sensor.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Spectrum_swell_Lagoon_blackman_336_pad_gaussian_filter.png} 
\caption{Spectrum of the swell wave significant height in the lagoon, filtered with a blackman filter and smoothed with a gaussian filter with three sigma.}
\label{swellspectrumlagoon}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Spectrum_Very_Low_Frequency_lagoon_pad_336_blackman_gaussian_filtering.png} 
\caption{Spectrum of the very low frequency waves significant height in the lagoon, filtered with a blackman filter and smoothed with a gaussian filter with three sigma.}
\label{VLFspectrumlagoon}
\end{figure}

In the figure \ref{swellspectrumlagoon} and \ref{VLFspectrumlagoon}, we see the impact of the modulation on the swell frequencies and the VLF frequencies. We see that the have a strong diurnal, semi-diurnal and some compound peaks for the swell while the VLF only exhibit semi-diurnal variations. This would indicate that VLF are not linked to the same parameters, or is less sensitive to water depth.

\subsubsection{Meteorological data}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Wind_velocity_spectrum_blackman_gaussian_3_sigma.png} 
\caption{Spectrum of the wind velocity during the campaign, filtered with a blackman filter and smoothed with a gaussian filter with 3 sigmas}
\label{windvelspectrum}
\end{figure}

In the figure \ref{windvelspectrum} we see that we have important diurnal and semi-diurnal components of atmospheric tides. Those could impact the circulation by local forcing on the currents but also on fetching currents from Noumea up to the lagoon of Ouano. Maybe I could get more inspired by looking at wind tidal ellipse.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Wind_direction_spectrum_blackman_gaussian_3_sigma.png} 
\caption{Spectrum of the wind direction during the campaign, filtered with a blackman filter and smoothed with a gaussian filter with 3 sigmas}
\label{winddirspectrum}
\end{figure}

In the figure \ref{winddirspectrum}, we only see a diurnal peak. This could be linked to thermal wind. \textcolor{blue}{However, I cannot explain the fact that we have semi-diurnal peaks for the velocity then. Maybe the velocity always works in the same direction ? I should definitely get wind tidal ellipses.}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Wind_temperature_spectrum_blackman_gaussian_3_sigma.png} 
\caption{Spectrum of the atmospheric pressure during the campaign, filtered with a blackman filter and smoothed with a gaussian filter with 3 sigmas}
\label{atmpressspectrum}
\end{figure}

On the figure \ref{atmpressspectrum} we see the spectrum for the atmospheric pressure. There, we see that the constant term is overly dominant, followed by the semi-diurnal and diurnal forcings. \textcolor{blue}{I also see a lot of peaks but I really think that they are due to the fact that I lacked some data and used an interpolation method.}
\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Spectrums/Wind_temperature_spectrum_blackman_gaussian_3_sigma.png}
\caption{Spectrum of the air temperature during the campaing, filtered with a blackman filter and smoothed with a gaussian filter with 3 sigmas}
\label{airtempspectrum} 
\end{figure}

We see on figure \ref{airtempspectrum} the spectrum for the air temperature. Once again, the constant term is overly dominant, followed by the diurnal term and then, interestingly, we have a semi-diurnal variation of temperature. \textcolor{blue}{It is unexpected actually. Maybe it is linked to the oceanic M2 tides and cold water it could bring up ? I should check up in the lagoon temperature signal.}

\subsection{Correlation and impacts}

\textcolor{blue}{Here I should try and compute the correlations between the wind, the elevation, the pressure and the waves. Maybe the temperature could be added since it could be linked with oceanic water, but I should be careful of upwellings. I would like to use it to determine what is the most important factor for the different types of circulation.}

\textcolor{blue}{To complete this, I could try and compute a quick computation of the power flux of the waves, the wind and air pressure to see the impact on elevation. But I should be careful about the transfer coefficients of the wind momentum to the see.}

\subsection{What do I wanna see with the data}

\textcolor{blue}{What are my main point ? I want to see how the fluxes of water through the reef appear to modify the tidal circulation. I think that it happens in two way. First by a direct impact on overelevation over the reef and in a region close to the reef. And afterward by inducing currents which have periods of the tide, but the phase of the elevation. This is not expected and through non-linear interaction, can even affect the residual circulation. The latter fact has been observed in \cite{chevalier2015impact} and also in the results of the report of Fabien Locatelli. There is even an article dedicated to this modulation \citep{bonneton2007tidal}. }

\textcolor{blue}{The question now, is, what do we want to observe ? And how do we think that we are going to observe it.}

\subsubsection{Observations for the M2/S2 tide}
\textcolor{blue}{We expect to see an overelevation close to the reef, that may have component modulated by the tides. This overelevation would cause a current which is modulated by the tide and whose phase is therefore the one of the overelevation and not of the current. We expect this influence to be especially strong close to where the breaking occurs on the reef, and when the waves are strong.}


W\textcolor{blue}{We expect the breaking to occur at different places of the reef according to the origin of the swell, and then to have currents exhibiting different pattern. More western swell would rather break on the North-Western part of the reef, and therefore affect more the current in the North and Ouarai pass. Swell coming rather from the South are expected to break close to the center of the reef, and therefore to generate elevation close to our sensors array. The currents induced by this elevation are more likely to be detected at Point Fixe, and the Digoro passages, while the more affected pass would be the Isié one.}

\textcolor{blue}{The incoming direction would have a second effect. The incoming wave comes with a given quantity of momentum that gets transfer to the mean flow. We therefore expect the mean flow to inherit a part of this momentum. Since western waves carry mostly momentum to the east, we expect the eastern waves to have more influence in the eastern part of the lagoon, and to go closer to the coast. While for more south-eastern wave, we expect the flow to have more effect on the northern part of the lagoon, and to have an influence that is restricted to a zone closer to the reef.\footnote{Those observations work with the findings of \cite{sous2017circulation}.}}

\textcolor{blue}{By looking at the data though, I could not find any link with the neap tide / spring tide modulation and the general patterns of circulations. It really seems that the main circulation drivers are at first order the wind and the wave-breaking.}

\subsubsection{Observations for the K1 tide}
\textcolor{blue}{Surprisingly, the data show a strong K1 signal for the wind. This could have an impact on the diurnal tides of the lagoon. Even more, the asymmetry of the wind signal, that is strong in the North-West direction but nearly null to the South-East, may explain the fact that the one day averaged currents are mostly incoming by the Tenia pass. The period of strong wind could also affect the rest of the circulation, by imposing a mean current to the North-West. This current could stop the wave-induced current and even advect them to the North of the lagoon. Note that the strength of that current may arise from the fact that it has fetched from Noumea, which is a $100 km$ upwind.}

\subsection*{Note on the Isié and Ouarai passes}
\textcolor{blue}{One must note that right now, we do not have any explanation for the Southward Pattern and the Isié reversal patterns. Those may be linked with the positioning of the sensors in the pass. The lateral positions of sensors may actually cause them to be prone to detect incoming tidal residual, by a mechanism of repartition of residuals along the pass that favors outgoing residual in the center and incoming residual at the edges. This mechanism detailed by \cite{zimmerman1981dynamics}, and shown on the figure \ref{Currentresidualpass}.}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{Coeur_de_these/Materiel_methodes/Campagne_2016/FIGURES/Tidal_Residual_passes_Zimmerman_1981.png} 
\caption{Scheme of generation of tidal residual in a cul-de-sac, taken from \cite{zimmerman1981dynamics}. In the case of a pass, the lower velocity side would be the oceanic one and the higher velocity side the lagoon one. Therefore, we expect net flow residual on the side of the passes and net ebb residual on the center on this pass. The position of the sensors would therefore have an important impact.}
\label{Currentresidualpass}
\end{figure}

\textcolor{blue}{Note that this effect is hard to observe with our current model. Because our resolution in the passes is too low to resolve such flows.}

\textcolor{blue}{Other possible explanation is a failed filtering for the currents, because they actually only used low-pass filters but M2 signal may still have leaked. Also physically, it may be that very strong currents created by wave-breaking on the North of the lagoon lead to Venturi effects and pump in water from the ocean through the Isie and Ouarai pass. The other possibility is that the current parallel to the lagoon is deviated to the East by the Coriolis forces, and comes into the lagoon.}

\textcolor{blue}{Actually there is a last, and way better explanation. It is that the tidal residual in lagoon passes is likely to be incoming. To see how it this is possible, let's first have a look at the bathymetry between the ocean and the lagoon, that crosses the pass. }

\begin{figure}[h]
\label{Passesverticalslice}
\begin{tikzpicture}
\draw [thick] (0,0) -- (7,4) -- (12,4.5); % The bathymetry
\draw [dashed] (7,4) -- (7,5) -- (8,5) -- (8,4.1); % The reef
% Now I do small arrows to represent the current
\foreach \y in {0.5,1,1.5,2,2.5,3,3.5,4,4.5,5}
    \draw [->] (0.2,\y) -- (0.3,\y); % At ocean depth
    
\foreach \y in {1.5,2,2.5,3,3.5,4,4.5,5}
    \draw [->] (2,\y) -- (2.2,\y);  % Closing in
    
\foreach \y in {3,3.5,4,4.5,5}
    \draw [->] (4,\y) -- (4.7,\y);
    
\foreach \y in {4,4.5,5}
    \draw [->] (6,\y) -- (6.9,\y);
    
\foreach \y in {4.5,5}
    \draw [->] (8.5,\y) -- (9.6,\y);  
    
%%% And I add name tags
\node[draw] at (3,6) {Ocean};
\node[draw] at (7.5,6) {Reef};
\node[draw] at (10,6) {Lagoon};
\end{tikzpicture}
\caption{Scheme of an entering current in a lagoon pass. }
\end{figure}

To understand the flow into the pass, we can use the equations for the momentum and the mass conservation in one-dimension. It neglects the lateral shear but it is sufficient for our purpose. We then have the following system :

\begin{align}
\label{System1Dpass}
\frac{\partial (Hu)}{\partial t} + \frac{1}{2} \overrightarrow{\nabla}(Hu^{2}) + \overrightarrow{\nabla} P +\overrightarrow{F}= 0\\
\frac{\partial \eta}{\partial t} + \overrightarrow{\nabla}(Hu) =0
\end{align}

Since the variation of elevation are going to be reduced compared to the total current, we can consider that the flow is incompressible and that $\overrightarrow{\nabla} (Hu)$ is nil. Therefore, we have the following relation for the velocity : 

\begin{align*}
H(x)u(x) =  cst 
\end{align*}

Which, under division by $H(x)$ leads to :

\begin{align}
\label{Evolutionvelocitypass}
u(x)=\frac{cst}{H(x)}
\end{align}

Here $cst$ represents the flow of water. If we neglect the horizontal variations of the flow, we can modify the equation of momentum of \ref{System1Dpass} to obtain :

\begin{align}
\label{Modified1Dpass}
\frac{\partial(Hu)}{\partial t} + Hu \frac{\partial u}{\partial x} + \overrightarrow{\nabla} P + \overrightarrow{F}=0 
\end{align}

Now, we can see that in the case of our pass, the depth decreases when getting to the lagoon. By using a reference of x increasing in the direction of the lagoon, we can use a linear parametrization of the depth :

\begin{align}
\label{depthpassparametrization}
H(x)=H_{max}-bx
\end{align}

Where $b$ is the slope and $H_{max}$ the reference depth. Combining this relation with the equation \ref{Evolutionvelocitypass}, We can estimate the value of the term $Hu\frac{\partial u}{\partial x}$ :

\begin{align}
Hu\frac{\partial u}{\partial x} = \frac{H(x) cste}{H(x)}\frac{d}{dx}(\frac{cste}{H_{max}-bx})=cste^{2}(\frac{-1}{H(x)^{2}} (-b)) = \frac{cste^{2}b}{H(x)^{2}}
\end{align}

And we see that this term does not depend on the sign of the flow and is always positive,which is the incoming direction. Therefore, by solving with an equilibrium with the friction, we can see that we end up with an entering current. However there maybe some more subtleties happening, notably by the interaction with the sides of the pass that will generate vorticity as well. But this is rather likely to generate discrepancies of residual in an axis perpendicular to the reef's axis \citep{obinson1983tidally}.

If the observed flow in Isie and Ouarai was indeed the incoming residual, we should observe variation during the neap and spring tide cycle, since the the residual would be proportionnal to the square of the semidiurnal tidal velocity. Also, we would expect to observe it also at the Tenia pass. However, in Tenia the flow coming from the rest of the lagoon may also mask it.

Another possible explanation would come from a baroclinic gradient of pressure between the ocean and the lagoon. This could be driven by difference of water temperature that would be caused by differential heating. Since the renewal of water of the lagoons is likely to take several days and that the mixing of the lagoon water in the ocean is not very likely to change noticeably the temperature of the latter, we could have a difference of temperature. We could have two indicators for this case. First, we would expect a very different stratification of the flow in the pass compared with the barotropic case. Indeed, the pressure gradient would actually grow with depth, and therefore we could maybe observe bottom currents still entering in the lagoon during the ebb phase of the tidal cycle, and maybe a stronger bottom current than expected. The second signal would be a variation of temperature of the lagoon water with semi-diurnal tidal period, which could account for the mixing with oceanic water. But this could also be simply a signature of the diurnal heating of the lagoon by the sun.

However, it is hard to combine this information with the angle of incoming swell. Actually, if swell broke on the reef of the North-Western part of the lagoon, we would expect it to lead to a current going into both the Digoro and going out of Isié and Ouarai. One possibility is that if the current is strong enough, it has enough momentum to pass before Isié and Ouarai and going on in the lagoon, and generating vorticity doing so. The interaction of this vorticity with the tidal flow in Isié could therefore end up in a incoming current in Isié. Therefore, we would expect this with a flow through Digoro. Also, it seems logical in a sense since the Coriolis force is likely to deflect the current towards Tenia. \textcolor{blue}{And so I should be able to check this numerically}

A last possible explanation is that the swell is  focused through those two passes and finally generates a net incoming current by Stokes drift. \textcolor{blue}{But I should check that because intuitively I would expect the Stokes drift to be opposed to the wave propagation. Especially since in that case the swell do not break and we therefore have a pressure gradient that goes outward of the lagoon. Or maybe they cannot break but be dissipated by friction or diffraction. And this would lead to a net current towards the direction of the lagoon.}
\subsection{Differences of elevation at N'Digoro}

\textcolor{blue}{At the two N'Digoro passes, we observe differences of elevations. This is peculiar because they are very close. The two possible explanation are a possible effect of the wave-breaking modulation, an artefact in the data or an effect of the mangrove. The mangrove is actually an intertidal zone and could act as a tidal flat. This would also explain the absence of many harmonics in the signal. Because the tidal flats can have a smoothing effect on the tidal signal.}

\subsection{Weak elevation at the barrier reef}

\textcolor{blue}{By looking at the elevation data on the barrier reef, I observed that those were a little weaker than they are at other places. Uncertainties in the data are of course the main suspect for this effect. But there could be two other phenomenons that leads to that effect. The first one is that we could have an important advection term that, by Venturi effect, weakens the amplitude. I can explain that with our standard system of equations, and by only considering a constant term and a semi-diurnal one :}

\begin{align}
\label{Venturieffectreefsystem}
\frac{\partial \overrightarrow{v}}{\partial t} + \frac{1}{2} (\overrightarrow{v}^2) + g \overrightarrow{\nabla} \eta &= \frac{r\overrightarrow{v}}{H+\eta} \\
\frac{\partial \eta}{\partial t} + \overrightarrow{\nabla}( (H+\eta)\overrightarrow{v}) = 0
\end{align}

\textcolor{blue}{If I allow myself to neglect the elevation $\eta$ compared to the mean depth, on account that is supposed to be little $\eta << H$ or that it would lead to third order interaction. And if I also neglect the residual from the non-linear interaction of semi-diurnal terms,  I can have the following system :}

\begin{align}
\label{Venturieffect developedsystem}
\frac{\partial \overrightarrow{v}_{M2}}{\partial t} + \frac{\overrightarrow{\nabla}}{2}(2 \overrightarrow{v}_{0}\overrightarrow{v}_{M2} +  \overrightarrow{v}_{0}^{2}) + g\overrightarrow{\nabla}(\eta_{0} + \eta_{M2})&=\frac{r}{H}(\overrightarrow{v}_{0}+\overrightarrow{v}_{M2})\\
\frac{\partial \eta_{M2}}{\partial t} + \overrightarrow{\nabla}(H(\overrightarrow{v}_{0} + \overrightarrow{v}_{M2})&=0
\end{align}

\textcolor{blue}{I can turn that system in two separated systems for the constant terms and the semidiurnal ones. I therefore end up with :}

\begin{align}
\label{Venturieffectconstantsystem}
\begin{cases}
-\frac{r}{H}\overrightarrow{v}_{0} + \overrightarrow{\nabla}(\frac{\overrightarrow{v}_{0}^2}{2} + g \eta_{0}) &= 0 \\
\overrightarrow{\nabla}(H\overrightarrow{v_{0}})&=0
\end{cases}
\end{align}

and 

\begin{align}
\label{VenturieffectM2system}
\begin{cases}
(\frac{\partial }{\partial t} - \frac{r}{H}) \overrightarrow{v}_{M2} + \overrightarrow{\nabla}(\overrightarrow{v}_{0}\overrightarrow{v}_{M2} + g\eta_{M2})&=0 \\
\frac{\partial \eta_{M2}}{\partial t} + \overrightarrow{\nabla}(H\overrightarrow{v}_{M2})&=0
\end{cases}
\end{align}

\textcolor{blue}{Both those systems can be combined in one equation for the elevation, which helps in seeing the effect of the advection term in a qualitative fashion. To do that we inject periodic solutions for our unknowns.}

\begin{align}
\label{VenturieffectEquationselevation}
\overrightarrow{\nabla}(\frac{H^{2}}{r}\overrightarrow{\nabla}(\frac{\overrightarrow{v}_{0}^{2}}{2} + g\eta_{0}))&=0 \\
-i\omega_{M2}\eta_{M2} + \overrightarrow{\nabla}(\frac{H}{i\omega_{M2}+r/H} \overrightarrow{\nabla}(\overrightarrow{v}_{0}\overrightarrow{v}_{M2} + g\eta_{M2}))&=0
\end{align}

\textcolor{blue}{Finally, we can get a glimpse at the effect of the advection term $\overrightarrow{v}_{0}\overrightarrow{v}_{M2}$ on the tidal amplitude on the reef. On the ocean side of the reef, we expect the velocities to increase towards the reef, because the depth is decreasing and we want to have a constant flow, and then this terms decreases on the shore side of the reef. Therefore we expect the laplacian of this term to have a maximum on the reef. For the elevation term, we expect it to increase continuously toward the coast, and therefore its laplacian does not exhibit a maximum. Actually, this should end up in a maximum of elevation on the reef.}

\textcolor{blue}{The second one is due to the gradient of both bathymetry and friction. In the Helmholtz equations for tidal elevation, we observe a term linked with the laplacian of amplitude, but also a term that is the scalar product of the elevation gradient and the friction gradient.  Over the reef, we expect the elevation gradient and friction gradient to have the same direction when coming from the ocean, but to have opposite signs when going to the lagoon. Therefore, this would have the same effect of increasing the amplitude compared to the ocean but reducing it compared to the inner lagoon.}

\textcolor{blue}{If we take the Helmholtz equation for just a tidal term, without advection of Coriolis, we have :}

\begin{align}
\label{SimpleHelmholtzsemidiurnal}
\eta_{M2} + \overrightarrow{\nabla}(\frac{iHg}{(i\omega_{M2}+r/H)\omega_{M2}} \overrightarrow{\nabla}(\eta_{M2}))=0
\end{align}

\textcolor{blue}{But I can split it by doing chain rule for derivation with the $\overrightarrow{\nabla}$ operator. I therefore end up with :}

\begin{align}
\label{Helmholtzsplittedsemidiurnal}
\eta_{M2} + \frac{ig}{\omega_{M2}} \overrightarrow{\nabla}(\frac{H}{i\omega_{M2}+r/H})\overrightarrow{\nabla}(\eta_{M2})+ \frac{iHg}{\omega_{M2}(i\omega_{M2} + r/H)} \Delta \eta_{M2}=0
\end{align}

\textcolor{blue}{If we focus on the amplitude and not on the phase lag, we can realize that the norm of $r/H$ is going to be very superior to the norm of the frequency $i\omega_{M2}$, and therefore we can simplify it in our case :}

\begin{align}
\label{SimplifiedHelmholtzequation}
\eta_{M2} + \frac{ig}{\omega_{M2}} \overrightarrow{\nabla}(\frac{H^{2}}{r})\overrightarrow{\nabla}\eta_{M2} + \frac{iH^{2}g}{\omega_{M2}r}\Delta \eta_{M2} = 0
\end{align}

\textcolor{blue}{Around the reef, we expect depth to vary between $1 m$ and $100 m$ offshore of the reef, while the friction coefficient would be of around $ 0.01 m.s^{-1}$ on the reef and $0.001 m.s^{-1}$ elsewhere. Therefore, we would have a value of $H^{2}/r$ of $10^7 m.s$ in the ocean and of $10^{4} m.s$, and this variation would happen over about $1 km$. Therefore, we expect the value of $\overrightarrow{\nabla}(H^{2}/r)$ to be of around $10 m.s$, and a value of $\overrightarrow{\nabla} \eta_{M2}$ of $10^{-5}$. To maintain the equality, we require a $\Delta \eta_{M2}$ of :}

\begin{align}
\label{DeltaetaM2}
\Delta \eta_{M2} = \mathcal{O}(\frac{\overrightarrow{\nabla}\eta_{M2}\overrightarrow{\nabla}(H^{2}/r)}{H^{2}/r}) = \mathcal{O}(10^{-7}) m^{-1}
\end{align}

\textcolor{blue}{This would lead to a variation of the slope of $10^{-4}$ per kilometer. It could be possible in case of a change of sign of the slope.}

\subsection{Bottom boundary layers}

\textcolor{blue}{Following what \cite{sous2017circulation} and other observations in lagoon, we cannot have direct observations for the boundary layer. However, if it is not too deep, we may be able to observe the three dimensionnal circulation that would be induced by wind or baroclinicly by internal waves.}

\subsection{Attack plan}

\textcolor{blue}{Now that everything has been set clear, we have to know what data we have. And with that, what we can observe.}

\begin{itemize}
\item \textcolor{blue}{Wind data from la Tontouta}
\item \textcolor{blue}{Pressure data from la Tontouta}
\item \textcolor{blue}{8 pressures sensors along the reef}
\item \textcolor{blue}{Wave modelling from Wavewatch 3}
\item \textcolor{blue}{Some pressure sensors we are not entirely sure of}
\item \textcolor{blue}{8 ADCP's into the lagoon}
\item \textcolor{blue}{Temperature data with the ADCP's}
\end{itemize}

\textcolor{blue}{And we have all that for about three months. Now we need a list of what we would like to observe.}

\begin{itemize}
\item \textcolor{blue}{We would like to see how the mean elevation evolves over the reef, and if it has some tidal signals. When we see mean elevation, we imply that we average over duration of at least half an hour}

\item\textcolor{blue}{ Why do we have so strong variations of amplitude on the reef and between the N'Digoro ? Is it a data artefact or something that has to do with the physics of the lagoon ?}

\item \textcolor{blue}{Can we observe different signal in constant and tidal velocities for different direction of incoming waves on the lagoon ? We need to find data for 15 days that are as close as possible, except for the incoming waves directions.}

\item \textcolor{blue}{Can we observe different patterns of tidal coefficients and ellipses for different amplitude of waves ? Can we see something in the phase signal of the tide ? To be sure that it is due to the forcing, I must note that the modulation due to diurnal tide for example or to barotropic variations are likely to have a common effects on all the sensors. While in my case I want relative discrepancies between the sensors.}

\item \textcolor{blue}{Can I see a link between some kind of velocities to the North and elevation and the intensity of the wind ?}
\end{itemize}

\subsection{Estimation of tidal and wave energy}

\textcolor{blue}{I made some quick estimates of the energy that is in the tide for the lagoon, in the waves and in the wind. }

\textcolor{blue}{For the tide, I use the potential energy of water brought. With a tidal range of $0.8 m$ for the M2 tide and an area of the lagoon of about $3 10^{8} m^{2}$, we have an energy of about :}

\begin{align*}
E_{M2} = A*\delta \eta*g*\rho_{water} \approx 3.10^{8}*0.8*10*10^{3} \approx 2.4 10^{12} J
\end{align*}

\textcolor{blue}{For the waves, I can use the potential power transfer for a monochromatic wave. If I go for a amplitude of $1.5 m$ and a frequency of $10 s$, I can obtain a power density of\footnote{The $g^{2}$ term in this formula can be a little puzzling. But it comes from the fact that the power actually depends on the wave velocity. And this wave velocity also depends on the gravitationnal acceleration.} :}

\begin{align*}
P_{wave} = \frac{\rho_{water}g^{2}}{64 \pi} H_{m0}^{2}T_{e} \approx 1.125 10^{4} W.m^{-1}
\end{align*}

\textcolor{blue}{To compare this with the energy of the tide, I can multiply with the time of a tidal period and the length of reef over which this energy arrive. Since the reef is supposed to cover the length of the reef, we can use a length of $30 km$ and a semidiurnal period of $12h$ or $ 4.32 10^{5} s$. We then have :}

\begin{align*}
E_{wave} = 1.125 10^{4} * 3 10^{4} * 4.32 10^{5} \approx 1.5 10^{14} J
\end{align*}

\textcolor{blue}{We see that the wave energy is extremely important. Therefore, we could expect that modulation of this energy by the tide may affect the tidal pattern. But it heavily depends on the amount of that energy that is transferred into the mean flow, and that is not dissipated.}

\textcolor{blue}{Finally, to estimate the incoming energy from the wind, we have to estimate a realistic wind drag on the lagoon. We expect typical wind velocity of $2.5 m.s^{-1}$ and we will consider them constant to get a first order idea. We can go with the standard formula to estimate the wind drag :}

\begin{align*}
\tau_{wind} = C_{d} \rho_{air} U^{2}_{wind}
\end{align*}

\textcolor{blue}{Which, with a $C_{d}$ coefficient of $1.1 10^{-3}$ would give a stress of :}

\begin{align*}
\tau_{wind} = 1.1 10^{-3}*1*(2.5)^2 \approx 6.25 10^{-3} kg.m^{-1}.s^{-2}
\end{align*}

\textcolor{blue}{Here, we will not use this directly. But by neglecting Coriolis we can estimate the slope it would cause. And then we can use this slope to compute the additionnal volume of water, and the potential energy linked with this volume of water. The slope is given by :}

\begin{align*}
\frac{\partial \eta}{\partial x} = \frac{\tau_{wind}}{g \rho_{water} H} = \frac{6.25 10^{-3}}{10*10^{3}*10} \approx 6.25 10^{-8}
\end{align*}

\textcolor{blue}{We can already see that this slope is very weak, if we integrate this on the approximated thirty kilometers of the basin, we have an elevation difference of :}

\begin{align*}
\frac{\partial \eta}{\partial x}*L_{basin} = 6.25 10^{-8} * 3 10^{4} \approx 2 10^{3} m
\end{align*}

\textcolor{blue}{We therefore expect variations of the order of the millimeter in the lagoon. Note that since the wind stress is quadratic in the wind velocity, this could increase swiftly. However, the fastest wind velocity are of the order of $8 m.s^{-1}$\footnote{Actually, this kind of wind is supposed to be fairly common in the region. A modelling study of \cite{lefevre2010weather} showed that mean velocity of $8 m.s^{-1}$ where the most common during their measurement period.}; thus, it would only increase of one order and generate elevation of a centimeter. Anyway, we can compute the potential energy by using the lagoon with and the formulae for the area of a triangle, and so that the linear increase is equivalent to half the increase of the maximum elevation on the entire lagoon :}

\begin{align*}
E_{wind} = \delta  \eta * g* A* \rho_{water} \approx 10^{-3} * 10 * 3 * 10^{8} * 10^{3} = 3 10^{9} J
\end{align*}

\textcolor{blue}{Since we have variation of elevation that are one hundredth of the one of the tide, it is likely coherent. And we see that we do not expect the wind to have an important effect on the elevation. }

\textcolor{blue}{However, those calculations have only been made for equilibrium situations with a constant depth basin. In our case, the pictures is complicated because we do not only consider the local forcing, but also the current fetched by the wind from Noumea. Also, we are not always at equilibrum and have many inhomogeneities in the lagoon basin.}
