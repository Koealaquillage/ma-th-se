\section{Non-linear interactions between tides and cross-reef fluxes}

\textcolor{blue}{In this part, I want to discuss the non-linear interaction between the flux of momentum brought by the swell close to the barrier reef and the tides. More precisely, I want to estimate the part of the tidal overelevation and of the tidal currents that is brought by the presence of the swell. Optionnally, I would like to parametrize that. But this is obviously a complicated task because the problem is a non-linear one.}

\textcolor{blue}{Though, it is possible that the correction caused by the swell is small compared to the total tidal flux. In that case I could use a Taylor development to estimate the correction caused by the swell. This would also simplify the determination of uncertainties.}

\textcolor{blue}{I do not develop this theory from nothing. In the article of \cite{chevalier2015impact} they use a numerical model to investigate the hydrodynamic circulation linked both to the M2 component of the tide and the wave-breaking. They report that the exchange of water between the lagoon and the ocean averaged over a tidal period differ according to the importance of wave-breaking. This possibly points towards a coupling between the tide and the constant term of wave-breaking, since the parametrization of wave-breaking they used was constant.}

\textcolor{blue}{Also, there has already been report of a possible coupling between the M2 tide and a constant forcing. In \cite{jones2008modification} they studied the possibility in this coupling in the Northern sea in the presence of important wind. They concluded that this effect was possible and due to the advection and bottom friction terms of the St-Venant equations. Therefore, such effects would  be stronger in coastal regions and especially high-frictionnal regions such as coastal lagoons.}

\textcolor{blue}{This effect is vital for the estimation of tidal prism in the lagoon of Ouano. Since the quantity of water entering through the reef because of swell transformation and breaking is important, this could play a major role in modifying the tidal prism. This has been implicitly mentioned in \cite{chevalier2015impact} but no study has yet focused on the mecanism behind this coupling in coral lagoons or on the precise modification of the tidal prism caused by this. In this chapter, we explore possible paths to solve those issues.}

\subsection{Coupling in the St-Venant equations}
 
\textcolor{blue}{We use the St-Venant equations for the modelling of the M2 tide in the lagoon, since the associated flow is in first approximation two dimensional. We use the same equations as in \cite{jones2008modification} but we explicit the advection terms and neglect the variation of latitude. We therefore write it using a vector formulation as in equation \ref{StVenantjones2008} :}

\begin{align}
\begin{cases}
\frac{\partial \eta}{\partial t} + \overrightarrow{\nabla} \cdot ((H+\eta) \overrightarrow{u}) = O \\
\frac{\partial \overrightarrow{u}}{\partial t} + (\overrightarrow{u} \cdot \overrightarrow{\nabla}) \overrightarrow{u} + f \overrightarrow{e_z} \bigwedge \overrightarrow{u} =  - g\overrightarrow{\nabla} \eta + \frac{\overrightarrow{\tau^{s}} - \overrightarrow{\tau^{b}}}{\rho (H + \eta)}
\end{cases}
\label{StVenantjones2008}
\end{align}

\textcolor{blue}{Here, $\eta$ represents the elevation in $m$, $\overrightarrow{u}$ is the vertically averaged velocity in $m.s^-1$, $H$ the local depth in $m$, $f$ the Coriolis parameter in $s^-1$, $g$ the gravitationnal constant in $m.s^-2$. $\overrightarrow{\tau^s}$ and $\overrightarrow{\tau^b}$ are respectively the forcing brought by the wave-breaking or the wind and the bottom friction. For the bottom friction, we can use a second order in the velocity parametrization :}

\begin{align}
\overrightarrow{\tau^b} = k\rho |\overrightarrow{u}| \overrightarrow{u}
\label{bottomfrictionsecondorder}
\end{align}

\textcolor{blue}{Where $|\overrightarrow{u}$ is the norm of the velocity vector. }

\textcolor{blue}{For the surface forcing, we can use the parametrization of the flux given in \cite{chevalier2015impact} which depends linearly on the water level $\eta$, with a slope determined by the wave height.}

\begin{align}
\overrightarrow{\tau^s} = A(H+\eta) +b
\label{forcingwavebreaking}
\end{align}

\textcolor{blue}{Finally, if we inject those two relations in the system \ref{StVenantjones2008}, we obtain the system \ref{StVenantwavebreaking}.}

\begin{align}
\begin{cases}
\frac{\partial \eta}{\partial t} + \overrightarrow{\nabla} \cdot ((H+\eta) \overrightarrow{u}) = O \\
\frac{\partial \overrightarrow{u}}{\partial t} + (\overrightarrow{u} \cdot \overrightarrow{\nabla}) \overrightarrow{u} + f \overrightarrow{e_z} \bigwedge \overrightarrow{u} =  - g\overrightarrow{\nabla} \eta + \frac{1}{\rho} A + \frac{b}{\rho (H + \eta)} - \frac{k|\overrightarrow{u}|\overrightarrow{u}}{(H + \eta)}
\end{cases}
\label{StVenantwavebreaking}
\end{align}

\textcolor{blue}{We therefore have explicitly three non-linear terms and the denominator $(H + \eta)$ may be hard to handle. Most of the time, this denominator is simplified to $H$ because we have a total depth $H$ largely superior to the tidal variations. However, in the case of a coral lagoon, the depth over the reef barrier is approximately equal to the tidal range. Since it is expected that most of the interaction between wave-breaking and tide would occur linearly, this term could hardly be neglected. We then multiply the equation of momentum by $(H+\eta)$ and obtain the following momentum equation \ref{momentumequationwavebreaking}.}

\begin{align}
(H+\eta)(\frac{\partial \overrightarrow{u}}{\partial t} + (\overrightarrow{u} \cdot \overrightarrow{\nabla}) \overrightarrow{u} + f \overrightarrow{e_z} \bigwedge \overrightarrow{u}) =  (H + \eta)(- g\overrightarrow{\nabla} \eta + \frac{1}{\rho} A) + \frac{b}{\rho} - k|\overrightarrow{u}|\overrightarrow{u}
\label{momentumequationwavebreaking}
\end{align}

\textcolor{blue}{This gives rise to a entirely non-linear system. But before we try to detail the solution, we can notice the term $\eta A$ which directly couples the wave-breaking flux to the M2 tidal level. But it is not the only way coupling occurs between the constant wave breaking and the tidal level and currents.}

\subsection{Analytical developments}

\textcolor{blue}{If we consider that the constant and M2 terms are the dominant ones in this equation, we can develop the solutions with only those terms.}

\begin{align}
\eta = \eta_0 + \eta_{M2} \\
\overrightarrow{u} = \overrightarrow{u_0} + \overrightarrow{u_{M2}}
\label{developmentsolutionM2constant}
\end{align}

\textcolor{blue}{Here the terms corresponding to the M2 tidal component are periodic and therefore are of the form $\eta_{M2}cos(\omega t)$ where $\omega$ is the frequency of the M2 tidal component. In this case, we see that the product of a constant term and a term of M2 frequency leads to a term of M2 frequency, the product of two constant terms leads to a constant terms and the product of two terms of M2 frequency leads to a constant term and a term of M4 frequency that we will not consider here.}

\begin{align*}
A_0 B_0 = A_0 B_0 \\
A_0 B_{M2} cos(\omega t) = A_0 B_{M2} cos(\omega t) \\
A_{M2} cos(\omega t) B_{M2} cos(\omega t) = A_{M2} B_{M2} ( cos(2\omega t) + 1 )/2 
\end{align*}

\textcolor{blue}{If we inject those in equations \ref{momentumequationwavebreaking}, we obtain the following system for the constant terms :}

\begin{align}
\begin{cases}
\overrightarrow{\nabla} (H \overrightarrow{u_0} + \frac{\eta_{M2} \overrightarrow{u}_{M2}}{2}) = 0 \\
\frac{\eta_{M2}}{2}\frac{\partial \overrightarrow{u}_{M2}}{\partial t} + \frac{H}{2} (\overrightarrow{u}_{M2} \cdot \overrightarrow{\nabla} ) \overrightarrow{u}_{M2} + \frac{\eta_{M2}} (\overrightarrow{u_{0}} \cdot \overrightarrow{\nabla} ) \overrightarrow{u}_{M2} + \frac{\eta_{M2}}{2} (\overrightarrow{u}_{M2} \cdot \overrightarrow{\nabla} ) \overrightarrow{u_{0}} + \\ H (\overrightarrow{u_{0}} \cdot \overrightarrow{\nabla} ) \overrightarrow{u_{0}} + H f \overrightarrow{e_z} \bigwedge \overrightarrow{u_0} + \frac{\eta_{M2}}{2} f \overrightarrow{e_z} \bigwedge \overrightarrow{u}_{M2} = \frac{HA}{\rho} - \frac{\eta_{M2}}{2} g \overrightarrow{\nabla} \eta_{M2} + \frac{b}{\rho} + \overrightarrow{fric}_0
\end{cases}
\label{constanttermstvenant}
\end{align}

\textcolor{blue}{We have not touched the friction terms because the absolute value is notoriously difficult to develop, and such an effort would not be necessary here. If we regroup all the advection terms under the term $\overrightarrow{adv}_0$ and decide to put all the non-linear terms on the right hand side, we have the system \ref{constanttermrhsstvenant}:}

\begin{align}
\begin{cases}
H \overrightarrow{u_0}  = -\frac{\eta_{M2}}{2} \overrightarrow{u}_{M2} \\
 H f \overrightarrow{e_z} \bigwedge \overrightarrow{u_0}  = \frac{HA}{\rho} + \frac{\eta_{M2}}{2} g \overrightarrow{\nabla} \eta_{M2} + \frac{b}{\rho} + \overrightarrow{fric}_0 - \overrightarrow{adv}_0 - \frac{\eta_{M2}}{2}\frac{\partial \overrightarrow{u}_{M2} - \frac{\eta_{M2}} f \overrightarrow{e_z} \bigwedge \overrightarrow{u}_{M2}}{\partial t}
\end{cases}
\label{constanttermrhsstvenant}
\end{align}

\textcolor{blue}{We see that non-linear interaction of the M2 tidal component can have an effect on the mean current and elevation. This is not entirely new, as for example \cite{zimmerman1978topographic} studied the generation of residual current by interactions of currents oscillating with a tidal frequency. But those interactions can also go the other way around and if we write the system for the M2 terms, we see that we also have terms arising from the interaction between M2 and constant terms :}

\begin{align}
\begin{cases}
\frac{\partial \overrightarrow{u}_{M2}}{\partial t} + \overrightarrow{\nabla}(H \overrightarrow{u}_{M2}) = - \overrightarrow{\nabla}( \eta_{M2} \overrightarrow{u}_{0})\\
H \frac{\partial \overrightarrow{u}_{M2}}{\partial t} + H f \overrightarrow{e_z} \bigwedge \overrightarrow{u}_{M2}  = - g H \overrightarrow{\nabla} \eta_{M2} + \eta_{M2}\frac{A}{\rho}  + \overrightarrow{fric}_{M2} - \overrightarrow{adv}_{M2} - \eta_{M2} f \overrightarrow{e_z} \bigwedge \overrightarrow{u_0}
\end{cases}
\label{M2termrhsstvenant}
\end{align}

\textcolor{blue}{Most of the present terms are generic non-linear coupling terms. But the $\eta_{M2} \frac{A}{\rho}$ term is specific to our wave-breaking problem. Therefore, it is the one to which we are going to give a special attention.}

\subsection{Development of the friction terms}

\subsection{Experiments with CROCO}

\textcolor{blue}{Here, we tried to determine the importance of the coupling of the M2 tide with the wave-breaking by using it in the CROCO model. We try to compare the results obtained with and without the wave-breaking with the in-situ data that we have, and therefore to estimate if whether or not it plays a role. Also, we estimate the impact of the wave-breaking on the tidal prism.}

\textcolor{blue}{One other interesting thing would be to study the spatial repartition of water flux on the barrier and its impact on the lagoon circulation. And the last point would be to play with friction to see how much it influences the coupling.}

\textcolor{blue}{Finally, we have to consider the possible impact of the wind and the boundary condition and of the M2 spectrum of the waves. For that I have to gather the data and analyze how much energy wind, boundary conditions and waves contain. And see with the model if those parameters are likely to influence the tidal prism and the outputs at the level of my sensors as well.}

\subsection{Stuff I should try}

\textcolor{blue}{I should definitely try to do some tidal analysis but for different periods of the data I have. Because the wave regime has had important variations, as well as the wind regime. Therefore, I should expect to see those variations. And I do not know why but I expect to have clearer results with the velocities than with the amplitudes. But I should go a little deeper into tides physics to understand that.}