\section{Circulation du lagon de Ouano}

The hydrodynamics and water circulation in the Ouano lagoon has been studied by \cite{chevalier2015impact} for its numerical aspects and \cite{sous2019wave} for the transformation of the swell over the barrier reef. Other studies were done on the lagoon, but did not explicitly aim this lagoon. Rather they tried to study the entire complex of lagoons of the South-Western part of New-Caledonia. 

\subsection{Circulation in southern New Caledonia}



\textcolor{blue}{For example, in \cite{jouon2006calculations} they use numerical models to study the different hydrodynamics time parameters in the South-West lagoon of New-Caledonia. While in \cite{ouillon2010circulation} they model the circulation and the possible patterns for sediment deposition in the entire lagoon. The impact of larger scale of circulation in New-Caledonia has been detailed in \cite{marchesiello2010coastal}, especially the impact of the Alis current on upwelling and its location.}

\textcolor{blue}{Longer terms studies have also been done in the lagoon. For example, \cite{ouillon2005enhancement} studied the variability in salinity and temperature in the Noumea lagoon. They observed variations, both seasonal and linked to the El Nino Southern Oscillation (ENSO) of salinity and temperature. The seasonal variations were pictured as reduced compared to the ones caused by ENSO. They also pointed out that variations were enhanced in coastal regions. This could partially be explained by the longer residence times observed in \cite{jouon2006calculations}, as in that case, the freshwater coming from the river would rather stay close the coast, and the effects of evaporation and solar heating would be stronger in those confined water as well.}

\textcolor{blue}{A longer analysis of the biogeochemical parameters of the South-West lagoon can be found in \cite{le2010hydrography}. This study is rather centered on a small part of the South-West Lagoon of New-Caledonia but some parameters are registred during several decades. This allows one to see climate change variation or seasonal trend in the temperature in the lagoon. Also, some of the links between the parameters in this part of the lagoon might still be applicable to the Ouano lagoon.}

\textcolor{blue}{Interesting observations about the bathymetry between the ocean and the reefs and the upwelling it induces can be found in \cite{ganachaud2010observed}. In that case, they link the upwellings with both the winds and the encounter of two large scales currents in the southern reef. Finally, they conclude that the very steep bathymetry make the zone a prominent upwelling zone.}

\textcolor{blue}{A deeper study on the meteorological system of New-Caledonia can be found in \cite{lefevre2010weather}. The bigger points are the importance of trade winds, the impacts of the mountain on the thermal breeze between land and ocean and finally, the strong variations induced by ENSO.}

\textcolor{blue}{Finally, some tracers can also be used to infer the hydrodynamics. For example, in the study of \cite{cuif2014wind} we can trace the disperal of coral larvae to the wind patterns and the movement of water that they induce. But more involved modelling have been done in \cite{fuchs2012modelling} in case of a La Nina event. And in the general setting, more involved 3D modelling has been done in \cite{faure2010modelling}.}

\subsection{Circulation in the Ouano Lagoon}

The circulation directly into the Ouano lagoon has been studied in four main campaigns, in 2011, 2013, 2015 and 2016. The bathymetry had however been measured before, even though some new measurements were made for the 2016 campaign. The main objective was to study the hydrodynamics and we assumed that the effects of salinity and temperature were negligible. This is not always the case in coral lagoons \citep{rogers2016thermodynamics} but it seems to be a good first estimate in the present case.

A more extensive study was done about the 2016 campaigns by Damien Sous and in the report of Fabien Locatelli, of Seatech. This study was about both the wave transformations and the circulation patterns in Ouano. In \cite{sous2017circulation} and in the report of F. Locatelli, they identified four main circulation patterns in the lagoon, associated with where water goes in or goes out. Those are identified on the figure \ref{CirculationpatternOuano}.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Circulation_patterns_Sous_2017.png}
\caption{The four main circulation patterns observed in the Ouano lagoon during the 2016 campaign. Taken from \cite{sous2017circulation}}
\label{CirculationpatternOuano} 
\end{figure}

The "Classic Pattern" is similar to the pattern observed in Tulear lagoon in \cite{chevalier2015hydrodynamics} in another lagoon in Madagascar. In this pattern the water flows through the barrier reef and leaves the lagoon by the passes. The other patterns are more surprising because they have different for entering and exiting water. Even more interestingly, those patterns are studied in \cite{sous2017circulation} and the report of F. Locatelli because they are associated with different conditions of wind and waves mainly.

The Tenia reversal pattern is close to the classical patterns but water flows into the lagoon from the ocean and the other lagoons. This pattern is correlated with stronger south-east winds and waves from the south-west. Therefore, the long fetch of the wind over the entire lagoon complex induce a stronger current parallel to the shore, which goes into the lagoon. And the direction of incidence of the waves on the barrier creates a current with a more important component parallel to the reef.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Classical_Tenia_Pattern_Sous_2017.png}
\caption{Example of wind, current, and wave forcing during an episode of a) Classical Pattern and b) Tenia reversal Pattern. Taken from \cite{sous2017circulation}}
\label{CirculationpatternCPReversedTeniaOuano}
\end{figure}

The Isie reversal and Southwards patterns are close in their global functioning and the circumstances in which they happen. They both have water entering the lagoon by the Isie pass and the reef and leaving through Tenia pass. But in the Isie reversal pattern, the water also leaves by the Digoro pass, when it flows in in the case of the Southwards pattern. Both those patterns are linked with weak winds and waves and the lagoon seem to quickly switch from one pattern to another.

Some other situations were also reported in the report of F. Locatelli. For examples, some occurences of net currents going from the lagoon to the ocean through the reef. However, those situations were pretty rare, and were not really studied. Interestingly though, F. Locatelli found some correlations between the frequency of the peak of the incoming swell and the circulation pattern. He also found correlation between the wind and the amplitude of the swell of the day before and the circulation patterns in his classification, as is shown for example in figure \ref{Locatellireport}. Those facts introduced a possibility of more complicated relationship between incoming swell and current than was expected and also of possible time lag between forcing and circulation patterns. 

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Classification_Nord_Locatelli.png} 
\caption{Classification of the behaviour of the North pass according to external factors. Taken from the report of F. Locatelli}
\label{Locatellireport}
\end{figure}

The dependance on the frequency of the incoming swell may be due to the difference of friction exerted by the reef according to the frequency. In the paper of \cite{sous2019wave}, we see that different frequencies of the incoming swell are not attenuated of the same manneer on the reef. The infragravity (IG) and very low frequency (VLF) waves are less attenuated that the higher frequency swell waves, as in shown on figure \ref{WavetransformOuano}. Even more, since the breaking is related to wave steepness, it may be that lower frequency frequency waves present a lower steepness for the same amplitude and therefore break less. This would lead to less wave-driven current for higher frequency swell.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Tides_waves_transform_Sous_19.png}
\caption{Wave transformation according to the frequency band and the tidal stage. Taken from \cite{sous2019wave} }
\label{WavetransformOuano}
\end{figure}

The condition for wind, waves and current for the entire 2016 campaign are visible on the figure \ref{WindwaveOuano2016}. Here we see that we a clear mean direction of wave of around 200 degrees. There is also a fast oscillation of both wind amplitude and direction, with wind mainly breathing during the day and stopping at night. We see that wave amplitude has three strong peaks, while the wave period just vary with increase and plateau. For the currents, we see that the tidal variations are more important in Isie and Digoro than in Platier and Tenia. Also, if we look at the time-averaged currents, we see that water is always coming through in Platier, but is mostly going out in other stations.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Currents_wind_wave_Ouano.png}
\caption{Currents,wind and wave condition during the 2016 campaign in the Ouano lagoon. Taken from \cite{sous2019wave}} 
\label{WindwaveOuano2016}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Profondeur_lagon_Sous_19.png} 
\caption{Observation of the coral cover at low at tide a) over the barreer reef b) behind the reefs. Taken from \cite{sous2017circulation}}
\label{PictureCoralReef}
\end{figure}

In the article of \cite{bonneton2007tidal} they study the tidal modulation of wave-driven cross-reef in Aboré reef, in the south of the lagoon of Ouano. The depth of the two barreer reefs and the bathymetry as well as the wave regime can be compare. A cut of the bathymetry of the Ouano lagoon can be seen on figure \ref{WavetransformOuano} and for Aboré reef on figure \ref{Aborereefbathy}. In this article, they see a linear link between the wave-induced setup and the tidal elevation as can be seen on figure \ref{linktidalelevationwavesetupAbore}, as well as a linear link between the square of the current and the tidal elevation. \textcolor{blue}{But in this second case the uncertainties in the data can actually hide a linear link between the tidal elevation and the tidal current. Note that in the article they say that they have a modulation of the current at twice the frequency of the tide. However if you carefully inspect the signal of velocity above the reef, you see that it rather looks like a standard semi-diurnal component. This is shown on figure \ref{Tidalmodulationcrossreefflux}. Though on that figure we see way more variations for the wave setup than for the velocity. This could be the proof that the friction is indeed quadratic, because in that case important variations of velocities could be damped. It could also be the proof on should carefully choose the period to show, because the period shown for the velocity is much larger than the one shown for the wave setup; and it may be that the period shown for the wave setup is actually a period experiencing very important wave variability.}

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Tidal_modulation_reef_current_Bonneton_2007.png} 
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Tidal_modulation_reef_overelevation_Bonneton_2007.png} 
\caption{Tidal modulation of current and overelevation over the barreer reef in Abore. We see that the variations between tidal cycles are way stronger for the overelevation than they are for the velocity. I should look that up a little bit. Taken from \cite{bonneton2007tidal}}
\label{Tidalmodulationcrossreefflux}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Cross-reef-velocity-Abore-reef_Bonneton_2007.png} 
\caption{Linear link between the difference of depth between the breaking point and the wave setup. Taken from \cite{bonneton2007tidal}}
\label{bonneton2007tidal}
\end{figure} 

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/Circulation_a_Ouano/FIGURES/Bathymetry_Abore_Reef_Bonneton_2007.png} 
\caption{Bathymetry of the Aboré reef. Taken from \cite{bonneton2007tidal}}
\label{Aborereefbathy}
\end{figure}