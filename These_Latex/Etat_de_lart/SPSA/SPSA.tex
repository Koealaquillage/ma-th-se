\textcolor{blue}{\large I should add a really nice graph about the two phases of the SPSA. First the steep descent where you optimize most of the stuff but generates a lot of noise and second the slow phase where you mainly correct the noise}
\section{Simultaneous Perturbations Stochastic Approximation}
\subsection{SPSA and complex values}

The use of SPSA for functions with complex values argument is not straightforward.  The more common approach is to consider a decomposition of the complex arguments into a real and an imaginary part. This decomposition can be done by a linear mapping or with polar coordinates. Polar coordinates have the advantage of sometimes being physically more explicit. But in the case of parameters identification, they can turn a linear problem into a non-linear one.

This issue was raised in the field of quantum computing, which uses both complex number and SPSA algorithm. In \cite{utreras2019stochastic}, they use directly derivative in the complex variables. To do so, even in situation where the cost function is not holomorphic, they use the Wirtinger derivatives which extend some properties of the derivative in the complex domain. They also prove that the use of such a formalism of derivatives can enhance perfomance for parameters identification with the SPSA algorithm, as is shown on figure \ref{descentCSPSA}.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/CSPSA_Alarcon_2019.png}
\caption{Comparison of the efficiency of the CSPSA with the standard SPSA for a problem with complex arguments. Taken from \cite{utreras2019stochastic})}
\label{descentCSPSA} 
\end{figure}

\subsection{SPSA and high-dimensional problems}

The determination of parameters or the gradient descent with the SPSA algorithm in high-dimensional problem has raised some issues. Because of the curse of dimensionnality, the efficiency of the algorithm is highly dependant on the dimensions of the problem. This is why they did not use it directly on the parameters in \cite{hoang2014low} but use it to identify a parameter to approximate the parameters. In this situation, the millions of components of the covariance matrix would not have been directly estimable by any algorithm though.

In other situation, there are even more difficulties linked with the high-dimensionnality. In the  case of a high-number of parameters and high number of model outputs, we could expect that some parameters are linked with some outputs, but not with other. This is exactly the issue raised by \cite{antoniou2015w}. They realize that because of its form, the SPSA algorithm applied some mean perturbations to all the parameters, with no regards on the link between some outputs and parameters. Due to the very large scale of their problem, and the fact that outputs where actually highly decorrelated from most of the parameters, it gave them poor results.

To solve this, they decided to introduce the correlation between outputs and parameters by adding a matrix linking those. This matrix, formally, is actually the full jacobian matrix of the model outputs according to the parameters. However, it is often not available and has to be computed as an approximation. This could be done by computing it by finite differentiation or by building it during the parameters identification process. 

Interestingly though, this matrix also multiplies the covariance matrix of data error. The link with a possible smoothing of the possible solutions of the problem has not been explored, but could be interesting. This would join the effort on finding way to reduce the dimensionality of the parameters when using SPSA, which was done by \cite{tympakianaki2015c}. In their situation, they tried to group the parameters with high covariance and impose the same variations for all the group. This is formally equivalent to an approximation with constant functions of the parameters space


\subsubsection{Speeding up the gradient descent : Physical approach}
Maybe the main problem of the SPSA algorithm is that it has a rate of convergence inferior to a steepest gradient descent and is highly dependent of the number of parameters \citep{spall1998implementation}. The inferior rate of convergence is due to the noisy estimate of the gradient direction, however, this also brings up a greater robustness to noise in cost function estimate. For the dependance in the number of parameters, it is because, contrarily to the adjoint algorithm, there is no explicit relations between the parameters to modify and the data to optimize. This is directly due to the fact that the SPSA do not require any knowledge of the model.

It is quite handy not to require any information about the model, but sometimes we have a few information about the model. For example, we can know that some parameters are more likely to affect some parts of the model, or that some parts of the model are linked. This can be modelled by adding some correlation matrices in the cost function, as was done by \cite{tympakianaki2018robust}. In their article, they describe a model of vehicle traffic into a city, the controls being the timing of the traffic lights and the data some measurements of the fluxes of vehicles. They estimated a direct link between the proximity of traffic lights and the flux of vehicles and used it to optimize their model. 

This had the effect of reducing a second source source of slowness from the SPSA estimation. Since all the parameters are updated at once, and normally according only to the difference of cost function, which is a scalar, and of perturbations of parameters, which are determined at the beginning of the algorithm, the quality of the updates directly depends on the correlation of the parameters. But in model representing large scale phenomenon, we can expect a lot of measurements to be weakly correlated to most of the parameters. Therefore, performing the mean updates of the SPSA algorithm introduces a lot of uncorrelated noise to most of the data. The use of matrices of correlations between data and parameters can therefore greatly reduce the introduction of this noise, and speed up the convergence. An example of this speeding up can be seen on figure \ref{SpeedingupSPSACorrelation}. 

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/Hybrid_cSPSA_Tympakianaki_2018.png} 
\caption{Comparison of the standard SPSA with the SPSA with correlation matrix (c-SPSA). A SPSA with the gradient of the convexification term is also shown with the name Hybrid-C-SPSA. Taken from \cite{tympakianaki2018robust}}
\label{SpeedingupSPSACorrelation}
\end{figure}

Another interesting effect of this grouping was the reduction of the variability between gradient descent. Since the SPSA is a stochastic algorithm, the path taken for the gradient descent varies between to iterations of the algorithm. The convergence speed of the descent actually depends on this path, and this path can even lead in some cases to the algorithm being stuch in a local minimum \citep{chau2015overview}.The method of correlation matrices of \cite{tympakianaki2018robust} seems to actually decrease this dependance, as is shown in figure \ref{ReducingnoiseSPSA}.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/Random_7_cSPSA_Tympakianaki_2018.png} 
\caption{Comparison of different SPSA gradient descent, with or without correlations matrices. What we can see is a high variability in the descent of the standard SPSA but way less variability once the correlation matrices are introduced. Taken from \cite{tympakianaki2018robust}}
\label{ReducingnoiseSPSA}
\end{figure}

Another way to reduce this noise is to try to group the e parameters according to their influence on the model. This can naturally be done with the representer method of the adjoint method \citep{bennett1982open}, however, we sometimes do not have access to this method. Therefore, we can only rely on some models outputs to estimate the impact of parameters to the model, and group them accordingly, as was done in \cite{tympakianaki2015c}. In their situations, they used a k-mean clustering techniques to build some groups of parameters that would be updated as one, and therefore avoid the uncorrelated noise. 

Some researchers also proceed by performing a first analysis of the model, as was done by \cite{altaf2011simultaneous} and provide some linear estimates of the model. Then, those linear estimates can be used as basis function for the parameters, and a better outputs can be constructed from those basis function. Loosely speaking, it consists of an heuristic way to build a tangent linear model. This approach has been used with other algorithms than the SPSA algorithm, for example in \cite{lynch1998hindcasting} they used a SVD decomposition to find a basis for the boundary conditions and then reconstructed it for improving their outputs.

Finally, a last approach uses a physical knowledge of the model to be optimized to determine the grouping of the parameters. This has been done in \cite{boutet2015bottom}, where he actually correlated the bottom friction to the type of sediments which had been measured by the french Marine. This approach allowed to greatly reduce the number of parameters to be estimated, but did not give any correlations between the measurements and the parameters to be improved.

\subsection{Speeding up the gradient descent : Algorithmic approaches}

% I need something under subsubsection for here
\subsubsection{First order methods}

To speed up the convergence of the SPSA, many improvements of the SPSA algorithms were also considered. Those improvements could focus on the step size, on the direction of the descent or even on reducing the noise in the gradient estimate. Depending on the objective followed, all those things were not always wanted of followed, and there is an added difficulty in the fact that the estimator used in the SPSA are statistical.

If the problem is not too noisy, it can totally make sense to try and refine the gradient estimate. For this purpose there were some mean SPSA \citep{spall1997neural} and momentum SPSA \citep{spall1994nonlinear} introduced. The mean SPSA uses the mean of some descent direction around a given point to get a better estimation of the gradient. This has been proven to speed up the convergence in terms of iteration of the SPSA, but it also raises the cost in terms of iterations of the models, since it requires $n$ times more iterations of the model than the standard SPSA, $n$ being the number of estimates used in the mean process. Also, by reducing the noise in the gradient estimate, it may affect the robustness against noise in the estimates.

The momentum SPSA also uses an averaging process, but that does not increase the number of iterations of the model. In this case, the average is done with the previous available estimates of the gradient, with a weight given to those estimates that has to be tuned by the users, but must be inferior to 1. This is done to ensure that the algorithm do not diverge. 

This method has been inspired by a method used largely in Stochastic Gradient Descent (SGD) in machine learning. Therefore, different type of momentum method have been tried in SGD and were tested here, such as the Nesterov-momentum method \cite{sutskever2013importance} for example. This method uses a different point for estimating the gradient and updating the parameters, in order to disminish the zigzag that might occur in a gradient descent. We should however note that most of the methods were tested heuristically and that the actual almost sure convergence was never demonstrated.

The momentum method is actually interesting in its proximity with the conjugated gradients algorithm in its implementation, but very different in the objective pursued. In the conjugated gradient method, the main objective is to create a basis of vectors in the space given by the linear operator and the starting point and to find the weights of those vectors. Therefore, each step is conjugated or, more plainly said, orthogonal to the last step according to the curvature of zone studied. In the case of the momentum SPSA, the idea is to reduce the orthogonality between iterates, because it is supposed that this orthogonality is linked to noise in the estimate. The main link between those two methods is actually the reduction of the zigzagging in the last part of the gradient descent, compared to the case of steepest gradient descent. An example of this is shown on figure \ref{reducedzigzaginggradientdescent}

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/Zigzagging_nature_steepest_descent_wikipedia.png} 
\caption{Example of zigzaging in a valley caused the steepest gradient descent. Taken from wikipedia}
\label{reducedzigzaginggradientdescent}
\end{figure}

However, some methods for the estimation of the weight in the momentum technique can actually get it closer to the actual non-linear conjugate gradient method. The implementation of this for this SPSA has been envisaged, however, to issues are left unresolved. What is the robustness of the gradient conjugate to noisy estimates and how to tune the step-size accordingly ?

\subsubsection{ Second order methods}

Another methods, that has been more widely explore in the case of the SPSA is the building of an hessian estimation, or directly of its inverse. The idea is to used the perturbations found at the previous iterations to progressively build a hessian approximations, by supposing that the step are small enough not to change drastically the local curvature of the problem. This approach is evoked in \cite{zhu2002modified}, but they reduced it to a preconditionment problem. This come from the fact that the important noise encountered from the estimates gave very noisy hessian estimates, and therefore they were only able to find a good approximation of the diagonal terms in the hessian. This procedure relied highly on a new parameter used in the hessian approximation, as is shown on figure \ref{Hessianestimatezhu2002}. 

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/Gradient_descent_M2SPSA_Zhu_Spall_2002.png} 
\caption{Example of gradient descent of different SPSA, notably those with diagonal of the hessian estimates. Taken from \cite{zhu2002modified}}
\label{Hessianestimatezhu2002}
\end{figure}

Other methods were devised to provide an estimate of the inverse of the hessian. The one of \cite{bhatnagar2015simultaneous}. In their situation they used the Shermann-Morrison lemma to update directly the inverse of the hessian. They obtained good results, and, interestingly, proved the almost sure convergence of their algorithm towards a true estimate of the hessian, though it required certains conditions on the continuity of the cost function \footnote{Note that this requirement is frequent for gradient descent algorithm. The case presented here is $C^{3}$ but for the SPSA algorithm we in fact already need a continuous function. However, some procedures exist for non-continuous function, such as the convolution with a continuous function. This is described in more details in \cite{chau2015overview}.}.

Finally, the same authors improved their algorithm by proposing a new, non-symmetric perturbations to the parameters for the estimation of the gradients. Their work is presented in \cite{reddy2016improved} and in their case, the non-symmetric nature of their algorithm seemed to grant them better results. Also, in a very interesting manneer they prove that some of the condition on the distribution of the perturbation for the SPSA were too strong and they propose other distributions that might not respect it.

\begin{figure}
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/Comparison-SPSA-RDSA-Hessian.png} 
\caption{Evolution of the cost function with different kinds of SPSA and RDSA algorithm. Taken from \cite{reddy2016improved}}
\label{SecondorderRDSA}
\end{figure}

Contrarily to what is done with deterministic algorithm, the use of a hessian do not reduce the gain sequence to a line-search algorithm. The gain sequence is maintained in this case and the tuning of this gain sequence is still to be done according to the problem. But, the inverse estimated hessian can now be used for estimating the uncertainties in the parameters for example, even though one metric should be used for estimating the possible contribution to the noise in this hessian to the uncertainty estimated.

\subsubsection{Regularization and hybrid methods}

Another way to speed up the convergence is actually to reduce the size of the search space for the direction. This can be done by combining the stochastic estimation of the gradient with an analytic estimation of the gradient \citep{chau2015overview}, or by combining terms whose gradients can be computed analytically with other whose gradients cannot. This is the example of the Hybrid method used in \cite{tympakianaki2018robust}. In this case, the regularization part of the gradient is computed analytically while the rest is computed by the SPSA algorithm. The acceleration of the gradient descent is shown in the figure \ref{SpeedingupSPSACorrelation}. In other situation, this method can be used to improve the robustness of the gradient descent to the noise.

\subsection{Tuning the SPSA}
\subsubsection{Standard tuning}

The SPSA algorithm relies on a few parameters. Those are the step-size, which links the estimation of the gradient to the updates of the parameters, the perturbation coefficient, which is the size of the perturbations imposed on the parameters to estimate the gradient, and the parameters linked with their growth during the number of iterations. Some parameters can also be added, in the case of a hessian estimate for example \citep{zhu2002modified}, \citep{bhatnagar2015simultaneous}, or of a momentum estimation for the gradient \citep{spall1994nonlinear}. 

For some of those parameters, we dispose of reliable values for very diverse situations. This include the growth of the step size and perturbation, which tend to be common to most of the situation encountered \citep{bhatnagar2015simultaneous} \citep{spall1998implementation}. But for the initial values of those parameters, we still have to do manually tune it for every implementation, with only a few practical tips. 

Those practical tips are also harder to implement, since the perturbation and the step size are going to vary according to various facets of the problem, which may not been very well known. For example, the noise in the gradient estimate may lead to two successive gradients estimates having very different norms, regardless of the actual variation of the gradients at their location. Therefore, the step size should in this situation be something that is not too small, to that we actually move in the parameter space, but not too big so that we are not oversensitive to "overshooting issues". This variation is difficult to estimate at first, and most of the time trials and errors are necessary.

But even without this noise issue, the problem of the proportionnality between the cost function gradient estimate and the parameters update. This is also likely to vary with the varying of the perturbations in the course of the algorithm. Normally, this could be tackled by the use of hessian estimate or of a BFGS, even though the use of a line-search would still be required. But those are not straightly compatible with the SPSA, and therefore the step-size is still to be considered. 

A solution exist in the form of adaptable step sizes. In this situation, the step size is modified as the gradient descent advance. Most of the time, this step size if increased as long as the gradient direction do not change too much \cite{chau2015overview} and is shrunk if this changes radically. This too much change is formalized by two possible things : A cost function value that increases "too much", when it actually should not increase and a measurement of the angle between two successive gradients estimates that is too high. Both method are interesting, but still, an important work of tuning is important. This tuning is done on the tolerance for angle or increase of the cost function and the increase or decrease of step sizes in such cases. For the case of increase of cost function, a set of rules called Armijo of Wolfe conditions \cite{wolfe1969convergence} can be used. But they still require some tuning.

Another solution in the geophysics case is to consider the fact that we do have some informations that is not always available for optimization. In our case we also have the distance between each measurements and its associated model output. Therefore, we could adapt the step size as the value that would decrease as much as possible one those distance, or the mean of those distances, if the response of the model to the parameters was entirely linear. This would actually be like using a diagonal hessian estimate. This method presents actually two issues. First, the model can be highly non-linear, and depending exactly on what distance we want to reduce, we could introduce a lot of noise. Second, it may reduce the capacity of the algorithm to overcome noise in the cost function. But still, it could provide at least an order of value for the step-size. 

For the perturbations, the picture is slightly different. We know that with perturbations small enough, we will just reduce one of the two sources of noises in the gradient estimate. The first one is just that we use a directionnal derivative to mimick a gradient, and that will not be reduced. But the second part, the fact that the model is not entirely linear, will actually be reduced with small enough perturbations. Therefore, the size of perturbation would depend on the load of noise you want to impose on your model, but keeping in mind that too small perturbations may reach the threshold of epsilon value of your program. 

\subsection{Terminating the SPSA}

One of the main issue of the SPSA is also the stopping of the algorithm. Most of the time, gradient descent are stopped according to a criterion of reduction of the norm of the gradient. However, for the SPSA algorithm, this can be complicated by the fact that this gradient is very noisy. 

Therefore, the main approach is simply to fix a number of iterations that are supposed to be sufficient \citep{spall1998overview}. The estimation of what is "sufficient" can be helped by computations of the actual convergence rate, such as the one given in \cite{wada2013stopping}. Interestingly, we see that the convergence rate is between $n^{-1/2}$ and $n^{-2/3}$, but also that the reduction of cost function depends on the first distance with the optimum point, which is hard to tune. 

But this convergence rate is actually a reduction of the cost function. Therefore, we still have to get an idea of how much we want to reduce the cost function. In our case, we could have an answer to such a question, by looking at the weights of the cost function and the covariances of the different data that we have. We could try to stop the assimilation process once the outputs of the model are close to the measured data, where "close" mean at a distance of the square-root of the variance of the measured data. In this case, we would account for the uncertainties of the model by merging it with the data uncertainty. The question remain on how to include the regularization terms in this stopping criterion. However, the stopping criterion could be external and given when the outputs are close enough to the data, regardless of the regularization terms.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{Etat_de_lart/SPSA/FIGURES/Convergence_alpha_beta_Wada_Fujisaki_2013.png}
\caption{Example of convergence rate according to reduction wanted in the cost function. Taken from \cite{wada2013stopping}}
\label{Reductioncostfuncwada} 
\end{figure}
\subsection{SPSA in geophysics}

The SPSA algorithm was not initially designed for geophysical problems, but it found its place. By the fact that it is efficient, easy to implement and do not need access to the numerics of the problem to be solved, it could be easily implemented in a variety of problems. The first introduction was in \cite{hoang2014low} for the determination of a covariance matrix in a Kalman filtering problem. The problem was of very large scale and those covariance matrices are most often parametrized. In this case, the value of parameters can be determined by finite difference approximations. However, the SPSA algorithm proved itself more efficient than those approximation in this situation.

A more direct use in oceanography can be found in the thesis of Martial Boutet \citep{boutet2015estimation}. In his problem, he wanted to determine friction coefficients at the bottom of the Iroise sea using the SPSA algorithm. He realized that there were too many parameters and finally use a division of the sea bottom with the sediment nature. That made him reduce the size of his problem to seventeen parameters and allowed an improvement of the determination of the friction.

A last but interesting example in oceanography is the determination of depth. This was done in a twin experiments for the North Sea by \cite{altaf2011simultaneous}. To do so, they did not use directly their non-linear models but used approximations of this one by the use of POD decomposition. This was actually equivalent to the use of an ad-hoc tangent linear approximations, thus quite different of the approch of \cite{boutet2015estimation}.

\textcolor{blue}{Finally, one has to note that this algorithm is also widespread in the petroleum community. Its ease of implementation combined with the existence of commercial codes in this field made it a natural choice. An interesting example of application in this field can be found in \cite{rodriguez2006multiscale}.}


%-----------------------------------