( Sur l'air de "Stay the Night" de James Blunt)

Certains veulent connaitre, la couleur des sentiments,
Il y en a d'autres qui cherchent, l'origine du temps,
En fait, il y a autant d'obsessions que de gens

Mais moi je n'aime que toi, le linéaire tangent !

Tu caresses les courbes, tu effleures les monts,
Tu passes au-dessous de la tourbe, du plus profond vallon,
Tu nous permets de voir, au-delà des dimensions,
Et cela parce que tu es une linéarisation !

Avant de te connaitre, le monde était commplexe,
Et mon guide y était l'algorithme du simplexe,
Je faisais un petit tour, pour scruter les alentours,
Et je finissais dans un chemin de détours !

Mais quand je t'ai rencontré, tout ça a bien changé,
Car tu connais les chemins, et tous les petits recoins,
Et tu me montres la pente, sans embuche ni attente,

Tu caresses les courbes, tu effleures les monts,
Tu passes sous la tourbe, des plus profonds vallons,
Tu nous permets de voir, au delà des dimensions,
Et cela car tu es une linéarisation !

Mais au fond de la vallée, tu ne voulais plus bouger,
Tu étais allongée, parallèle aux coordonnées,
Dans une angoisse existentielle, tu regardais le ciel,
Te demandais si cette platitude n'était par artificielle !

Plus rien pour te tenter, pas même les gradients conjugués,
Tu étais toujours si habile, et là tu restais immobile,
Puis quand est arrivé, le recuit simulé,
J'ai vu ton oeil briller, et tes coefficients s'agiter !

Tu caresses les courbes, tu effleures les monts,
Tu passes sous la tourbe, des plus profonds vallons,
Tu nous permets de voir, au-delà des dimensions,
Et cela parce que tu es une linéarisation !

Et te voilà reparti, sur la topographie,
Tu as retrouvé ta voie, avec la SPSA,

Vous explorez les modèles, et leurs routes parallèles !

Tu caresses les courbes, tu effleures les monts,
Tu passes sous la tourbe, des plus profonds vallons,
Tu nous permets de voir, au-delà des dimensions,
Et cela parce que tu es une linéarsation !

Tu caresses les courbes, tu effleures les monts,
Tu passes sous la tourbe, des plus profonds vallons,
Tu nous permets de voir, au-delà des dimensions,
Et cela parce que tu es une linéarisation !
